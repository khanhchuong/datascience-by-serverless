import sbt.Keys._

name := "taxi-trips-prediction"

version := "1.0.3"

scalaVersion := "2.12.8"

val sparkVersion = "2.4.3"


libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % sparkVersion,
  "org.apache.spark" %% "spark-sql" % sparkVersion,
  "org.apache.spark" %% "spark-mllib" % sparkVersion,
  "org.apache.spark" %% "spark-avro" % sparkVersion,
)

