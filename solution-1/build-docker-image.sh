echo "Build and push docker image by using gcloud build"
sudo rm target/* -rf
gcloud auth activate-service-account --key-file /home/chuong/gcp.json
gcloud config set project chuong-demo
gcloud builds submit --tag gcr.io/chuong-demo/taxi-trips-prediction

echo "Done. Image is ready at gcr.io/chuong-demo/taxi-trips-prediction"

