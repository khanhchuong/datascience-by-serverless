echo "Remove sample data"
rm train/*
rm test/*

echo "Download dataset"
gsutil cp gs://chuong-demo-avro/taxi-trips-2015-000000000001.avro train/
gsutil cp gs://chuong-demo-avro/taxi-trips-2016-000000000001.avro test/
#gsutil cp gs://chuong-demo-avro/taxi-trips-2015-*.avro train/
#gsutil cp gs://chuong-demo-avro/taxi-trips-2016-*.avro test/


echo "Build and validate model"
sbt run
