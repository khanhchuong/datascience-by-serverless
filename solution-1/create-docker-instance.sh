echo "Create an instance in GCP"

gcloud compute --project=chuong-demo instances create chuong-demo-docker-instance --zone=europe-west1-b --machine-type=n1-standard-8 --subnet=default --network-tier=PREMIUM --maintenance-policy=MIGRATE --service-account=chuong-demo@chuong-demo.iam.gserviceaccount.com --scopes=https://www.googleapis.com/auth/cloud-platform --image=cos-73-11647-182-0 --image-project=cos-cloud --boot-disk-size=100GB --boot-disk-type=pd-ssd --boot-disk-device-name=chuong-demo-docker-instance

echo "ssh to the instance then run these commands"
echo "docker-credential-gcr configure-docker"
echo "docker run -it gcr.io/chuong-demo/taxi-trips-prediction:latest"

