#!/usr/bin/env bash
echo "download data"
mkdir train
gsutil cp gs://chuong-demo-avro/taxi-trips-2015* train/
mkdir test
gsutil cp gs://chuong-demo-avro/taxi-trips-2016* test/

echo 'run "sbt run" to build and evaluate model'
