import org.apache.spark.SparkConf
import org.apache.spark.ml.feature.VectorAssembler
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.ml.regression.LinearRegression

object Main {
  def main(args: Array[String]): Unit = {

    // create Spark Session
    val sparkConf: SparkConf = new SparkConf()
      .setMaster("local[*]")
      .set("spark.driver.memory", "4g")

    val sparkSession = SparkSession.builder.config(sparkConf).getOrCreate()
    sparkSession.sparkContext.setLogLevel("ERROR")

    // define features
    val assembler = new VectorAssembler()
      .setInputCols(Array("trip_distance", "passenger_count"))
      .setOutputCol("features")

    // get train and test data
    val dfTrain = assembler.transform(sparkSession.read.format("avro").load("train"))
    val dfTest = assembler.transform(sparkSession.read.format("avro").load("test"))

    // Define the model
    val lr = new LinearRegression()
      .setLabelCol("total_amount")
      .setFeaturesCol("features")

    // Fit the model
    val lrModel = lr.fit(dfTrain)

    // Evaluate the model
    val test_result = lrModel.evaluate(dfTest)

    println(s"\n\nroot Mean Squared Error:   ${test_result.rootMeanSquaredError}\n\n")

    sparkSession.stop()
  }
}


