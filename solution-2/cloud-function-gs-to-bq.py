import logging
from google.cloud import bigquery

GCP_PROJECT = "chuong-demo"
DATASET_NAME = "taxi"
TABLE_NAME = "data_update"

# Objective: Whenever there a new file in gs, we move it to a table in bq
def events_gs_to_bq_by_file(data, context):
    # get storage update data
    bucketname = data['bucket']
    filename = data['name']
    timeCreated = data['timeCreated']
    table_id = '%s.%s.%s' % (GCP_PROJECT, DATASET_NAME, TABLE_NAME)
    
    # log the receipt of the file
    uri = 'gs://%s/%s' % (bucketname, filename)
    print('Received file "%s" at %s. Ingest to: %s' % (
        uri,
        timeCreated,
        table_id
    ))

    # create bigquery client
    client = bigquery.Client()

    # get dataset reference
    dataset_ref = client.dataset(DATASET_NAME)

    # check if dataset exists, otherwise create
    try:
        client.get_dataset(dataset_ref)
    except Exception:
        logging.warning('Creating dataset: %s' % (DATASET_NAME))
        client.create_dataset(dataset_ref)

    # create a bigquery load job config
    job_config = bigquery.LoadJobConfig()
    job_config.autodetect = True
    job_config.create_disposition = 'CREATE_IF_NEEDED',
    job_config.source_format = 'AVRO',
    job_config.write_disposition = 'WRITE_APPEND',

    # create a bigquery load job
    try:
        load_job = client.load_table_from_uri(
            uri,
            table_id,
            job_config=job_config,
        )
        print('Load job: %s [%s]' % (
            load_job.job_id,
            table_id
            )
        )

        print('Load job is submitted (but we do not know when it is done) !!!')

    except Exception as e:
        logging.error('Failed to create load job: %s' % (e))
        
