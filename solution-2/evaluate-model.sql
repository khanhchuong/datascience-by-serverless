WITH
  eval_table AS (
  SELECT
    trip_distance,
    EXTRACT (DAYOFWEEK
    FROM
      TIMESTAMP_MICROS(pickup_datetime)) AS day,
    EXTRACT (HOUR
    FROM
      TIMESTAMP_MICROS(pickup_datetime)) AS hour,
    passenger_count,
    total_amount
  FROM
    `chuong-demo.taxi.test` )
SELECT
  *
FROM
  ML.EVALUATE(MODEL `taxi.model_lr`,
    TABLE eval_table)

