CREATE MODEL
  `taxi.model_lr`
OPTIONS
  (model_type='linear_reg',
    labels = ['total_amount']) AS
SELECT
  trip_distance,
  EXTRACT (DAYOFWEEK  FROM  TIMESTAMP_MICROS(pickup_datetime)) AS day,
  EXTRACT (HOUR  FROM  TIMESTAMP_MICROS(pickup_datetime)) AS hour,
  passenger_count,
  total_amount
FROM
  `chuong-demo.taxi.data_2015`

